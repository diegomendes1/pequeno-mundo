﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador : MonoBehaviour {
	public Jogo jogo;

	void Update(){
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			Debug.Log("esquerda");
			jogo.MoverCursor(0, -1);
		}else if(Input.GetKeyDown(KeyCode.RightArrow)){
			Debug.Log("direita");
			jogo.MoverCursor(0, 1);
		}else if(Input.GetKeyDown(KeyCode.UpArrow)){
			Debug.Log("cima");
			jogo.MoverCursor(-1, 0);
		}else if(Input.GetKeyDown(KeyCode.DownArrow)){
			Debug.Log("baixo");
			jogo.MoverCursor(1, 0);
		}
	}
}
