﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabuleiroInfo : MonoBehaviour {
	public int x, y;
	public int numMovimentos;

	public int GetX(){
		return this.x;
	}

	public int GetY(){
		return this.y;
	}

	public int GetNumMovimentos(){
		return this.numMovimentos;
	}
}
