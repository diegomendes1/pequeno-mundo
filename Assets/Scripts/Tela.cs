﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Tela : MonoBehaviour {
	public GameObject[,] tabuleiroTela;
	public GameObject[,] cursorTela;
	public GameObject pixelPrefab;
	public GridLayoutGroup tabuleiroHolder;
	public GridLayoutGroup cursorHolder;
	public Sprite cursor;

	public Cor[] totalCores;

	//UI
	public Image visualizador;
	public Text numMovimentosString;
	public Slider numMovimentosSlider;
	public Image estrela3, estrela2, estrela1;
	public Sprite estrelaSim, estrelaNao;

	//Telas de fim de jogo
	public GameObject ganhouJogoTela;
	public GameObject perdeuJogoTela;

	public GameObject[] estrelasFimJogo;

	public void GerarTabuleiro(int x, int y, Sprite corInicial){
		tabuleiroTela = new GameObject[x,y];
		tabuleiroHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(y, x);
		
		tabuleiroHolder.constraintCount = y;

		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				tabuleiroTela[i,j] = Instantiate(pixelPrefab);
				tabuleiroTela[i,j].GetComponent<Image>().sprite = corInicial;

				tabuleiroTela[i,j].transform.SetParent(tabuleiroHolder.transform);
				tabuleiroTela[i,j].transform.localScale = new Vector3(1,1,1);
			}
		}
	}

	public void GerarCursor(int x, int y, int posX, int posY){
		cursorTela = new GameObject[x,y];
		cursorHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(y, x);
		
		cursorHolder.constraintCount = y;

		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				cursorTela[i,j] = Instantiate(pixelPrefab);
				if(i == posX && j == posY){
					cursorTela[i,j].GetComponent<Image>().sprite = cursor;
				}else{
					cursorTela[i,j].GetComponent<Image>().color = new Color32(255,255,255,0);
				}
				
				cursorTela[i,j].transform.SetParent(cursorHolder.transform);
				cursorTela[i,j].transform.localScale = new Vector3(1,1,1);
			}
		}
	}

	public void MoverCursor(int novoX, int novoY, int antigoX, int antigoY){
		cursorTela[antigoX, antigoY].GetComponent<Image>().color = new Color32(255,255,255,0);
		cursorTela[novoX, novoY].GetComponent<Image>().sprite = cursor;
		cursorTela[novoX, novoY].GetComponent<Image>().color = new Color32(255,255,255,255);
	}

	public void MudarCor(int posX, int posY, Sprite novaCor){
		tabuleiroTela[posX, posY].GetComponent<Image>().sprite = novaCor;
	}

	public void MostrarInfoInicial(int totalNumMovimentos, string missaoImagem){
		numMovimentosString.text = "Movimentos Restantes: " + totalNumMovimentos;
		numMovimentosSlider.maxValue = totalNumMovimentos;
		numMovimentosSlider.value = totalNumMovimentos;
		estrela1.sprite = estrelaSim;
		estrela2.sprite = estrelaSim;
		estrela3.sprite = estrelaSim;
		Sprite missaoImg = Resources.Load<Sprite>("Missoes/Imagem/" + missaoImagem);

		visualizador.sprite = missaoImg;
		ganhouJogoTela.SetActive(false);
		perdeuJogoTela.SetActive(false);
	}

	public void DesativarEstrela2(){
		estrela2.sprite = estrelaNao;
	}

	public void DesativarEstrela3(){
		estrela3.sprite = estrelaNao;
	}

	public void AtualizarNumMovimentos(int numMovimentosRestantes, int totalNumMovimentos){
		numMovimentosSlider.value = numMovimentosRestantes;
		numMovimentosString.text = "Movimentos Restantes: " + numMovimentosRestantes;
		
	}

	public void SairJogo(){
		Destroy(GameObject.FindGameObjectWithTag("Inicio"));
		SceneManager.LoadScene("Menu");
	}

	public void ReiniciarJogo(){
		SceneManager.LoadScene("Game");
	}

	public void ProximoNivel(){
		GameObject.FindGameObjectWithTag("Inicio").GetComponent<DBManager>().PrepararProximoNivel();
		SceneManager.LoadScene("Game");
	}

	public void MostrarGanhouJogo(int numEstrelas){
		Debug.Log(numEstrelas);
		for(int i = 0; i < 3; i++){
			estrelasFimJogo[i].SetActive(false);
		}

		for(int i = 0; i < numEstrelas; i++){
			estrelasFimJogo[i].SetActive(true);
		}
		ganhouJogoTela.SetActive(true);
	}

	public void MostrarPerdeuJogo(){
		perdeuJogoTela.SetActive(true);
	}

	
}
