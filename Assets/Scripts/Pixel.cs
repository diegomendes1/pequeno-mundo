﻿public class Pixel{

	private Cor corAtual;
	public int corID;
	private int posX, posY;

	public Pixel(Cor corInicial, int posX, int posY){
		this.corAtual = corInicial;
		this.posX = posX;
		this.posY = posY;
	}

	public int GetPosX(){
		return this.posX;
	}

	public int GetPosY(){
		return this.posY;
	}

	public void SetPosX(int posX){
		this.posX = posX;
	}

	public void SetPosY(int posY){
		this.posY = posY;
	}

	public Cor GetCor(){
		return this.corAtual;
	}

	public void SetCor(Cor novaCor){
		this.corAtual = novaCor;
	}

	public int GetCorID(){
		return this.corID;
	}

	public void SetCorID(int corID){
		this.corID = corID;
	}
}
