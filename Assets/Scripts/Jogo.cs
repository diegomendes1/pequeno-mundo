﻿using UnityEngine;
using UnityEngine.UI;
public class Jogo : MonoBehaviour {
	public Tabuleiro tabuleiroAtual;
	
	public GameObject tabuleiroDesejado;
	public Tela tela;

	private Cor[] coresDisponiveis;
	
	public int numMovRestantes;
	public int totalNumMovimentos;
	private int cursorX, cursorY;
	public Cor corVazia;
	
	public int totalEstrelas;

	Inicio inicio;

	void Start(){
		totalEstrelas = 3;
		int tamanhoX, tamanhoY, numMovimentos;
		inicio = GameObject.FindGameObjectWithTag("Inicio").GetComponent<Inicio>();
		tamanhoX = inicio.tamanhoX;
		tamanhoY = inicio.tamanhoY;
		numMovimentos = inicio.numMovimentos;
		tabuleiroDesejado = inicio.tabuleiroDesejado;
		OrganizarCores(inicio.cores);
		IniciarJogo(tamanhoX, tamanhoY, numMovimentos);
	}

	public void IniciarJogo(int x, int y, int numMovimentos){
		totalNumMovimentos = numMovimentos;
		numMovRestantes = numMovimentos;
		tabuleiroAtual = new Tabuleiro(x, y, corVazia);
		tela.GerarTabuleiro(x, y, corVazia.Get());
		tela.GerarCursor(x,y, cursorX, cursorY);
		tela.MostrarInfoInicial(totalNumMovimentos, inicio.missaoImagem);
	}

	public void MoverCursor(int x, int y){
		int posX = cursorX + x, posY = cursorY + y;
		if(posX >= 0 && posX < tabuleiroAtual.GetX() && posY >= 0 && posY < tabuleiroAtual.GetY()){
			int corID = tabuleiroAtual.Get(cursorX, cursorY).GetCorID();
			
			tabuleiroAtual.Get(cursorX, cursorY).SetCor(coresDisponiveis[corID]);
			
			tela.MoverCursor(posX, posY, cursorX, cursorY);
			tela.MudarCor(cursorX, cursorY, coresDisponiveis[corID].Get());

			corID++;
			
			if(corID >= coresDisponiveis.Length){
				corID = 0;
			}

			tabuleiroAtual.Get(cursorX, cursorY).SetCorID(corID);

			cursorX = posX;
			cursorY = posY;
			numMovRestantes--;

			if(numMovRestantes <= totalNumMovimentos/4){ // uma estrela
				totalEstrelas = 1;
				tela.DesativarEstrela2();
			}else if(numMovRestantes <= totalNumMovimentos/2){//duas estrelas
				totalEstrelas = 2;
				tela.DesativarEstrela3();
			}
			
			if(ChecarTabuleiro()){
				GanhouJogo();
			}else if(numMovRestantes <= 0){
				PerdeuJogo();
			}else{
				tela.AtualizarNumMovimentos(numMovRestantes, totalNumMovimentos);
			}
		}
	}

	public bool ChecarTabuleiro(){
		int i = 0, j = 0;
		foreach(Transform t in tabuleiroDesejado.transform){
			//Debug.Log(t.GetComponent<Image>().sprite + ", " + tabuleiroAtual.Get(i,j).GetCor().Get());
			if(t.GetComponent<Image>().sprite != tabuleiroAtual.Get(i,j).GetCor().Get()){
				//t.GetComponent<Image>().sprite = coresDisponiveis[2].Get();
				
				return false;
			}
			//Debug.Log(t.GetComponent<Image>().sprite + ", - " + tabuleiroAtual.Get(i,j).GetCor().Get());
			j++;
			if(j >= tabuleiroAtual.GetX()){
				i++;
				j = 0;
			}
		}
		return true;
	}

	public void PerdeuJogo(){
		tela.MostrarPerdeuJogo();
	}

	public void GanhouJogo(){
		tela.MostrarGanhouJogo(totalEstrelas);
		SalvarJogo();
	}

	public Tabuleiro GetTabuleiroAtual(){
		return this.tabuleiroAtual;
	}

	public void OrganizarCores(int cores){
		string text = cores.ToString();
        string[] characters = new string[text.Length];

        for (int i = 0; i < text.Length; i++){
            characters[i] = System.Convert.ToString(text[i]);
        }

		coresDisponiveis = new Cor[characters.Length];
		for(int i = 0; i < characters.Length; i++){
			coresDisponiveis[i] = tela.totalCores[int.Parse(characters[i])-1];
		}
	}

	public void SalvarJogo(){
		GameObject.FindGameObjectWithTag("Inicio").GetComponent<DBManager>().SalvarMissaoConcluida(totalEstrelas);
	}
}
