﻿using UnityEngine;
public class Tabuleiro : MonoBehaviour{
	private int x, y;
	private Pixel[,] casas;

	public Pixel Get(int posX, int posY){
		return casas[posX,posY];
	}

	public Tabuleiro(int x, int y, Cor corVazia){
		this.x = x;
		this.y = y;
		casas = new Pixel[x,y];
		GerarTabuleiro(corVazia);
	}

	public void GerarTabuleiro(Cor corVazia){
		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				casas[i,j] = new Pixel(corVazia, i, j);
				casas[i,j].SetCorID(0);
			}
		}
	}

	public int GetX(){
		return this.x;
	}

	public int GetY(){
		return this.y;
	}

	public void SetCoordenadas(int x, int y){
		this.x = x;
		this.y = y;
	}

	public Pixel[,] GetCasas(){
		return this.casas;
	}

	public void SetCasas(Pixel[,] casas){
		this.casas = casas;
	}
}
