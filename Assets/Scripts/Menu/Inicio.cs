﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inicio : MonoBehaviour {
	public GameObject tabuleiroDesejado;
	public int numMovimentos;
	public int tamanhoX, tamanhoY;
	public string missaoImagem;
	public int cores;

	public int numMissao;
	public string nomeTipo;

	public void SetInfo(int numMovimentos, int tamanhoX, int tamanhoY, GameObject tabuleiro, string missaoImagem, int cores, int numMissao){
		this.numMovimentos = numMovimentos;
		this.tamanhoX = tamanhoX;
		this.tamanhoY = tamanhoY;
		this.tabuleiroDesejado = tabuleiro;
		this.missaoImagem = missaoImagem;
		this.cores = cores;
		this.numMissao = numMissao;
	}

	public void ColetarInfoDoArquivo(){
		this.numMovimentos = tabuleiroDesejado.GetComponent<TabuleiroInfo>().numMovimentos;
		this.tamanhoX = tabuleiroDesejado.GetComponent<TabuleiroInfo>().x;
		this.tamanhoY = tabuleiroDesejado.GetComponent<TabuleiroInfo>().y;
	}
}
