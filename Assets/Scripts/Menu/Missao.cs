﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Missao : MonoBehaviour {
	//UI
	public Text numMissaoText;
	public Image missaoImg;
	private string missaoImgName;
	public Image numEstrelasImg;
	//Dados

	public int numMissao;
	public string nomeTipo;
	public GameObject arquivo;
	public Button button;
	public int cores;

	public void SetMissao(int numMissao, string nomeTipo, string missaoImagem, int numEstrelas, string nomeArquivo, bool desbloqueado, int cores){
		this.numMissao = numMissao;
		this.nomeTipo = nomeTipo;

		numMissaoText.text = numMissao.ToString();
		numEstrelasImg.sprite = Resources.Load<Sprite>("Missoes/Estrela" + numEstrelas);
		missaoImg.sprite = Resources.Load<Sprite>("Missoes/Imagem/" + missaoImagem);
		arquivo = Resources.Load<GameObject>("Missoes/Dado/" + nomeArquivo);
		missaoImgName = missaoImagem;
		this.cores = cores;
		
		if(!desbloqueado){
			GameObject.FindGameObjectWithTag("Menu").GetComponent<Menu>().bloquearMissao(this.transform);
		}
		button.onClick.AddListener(delegate {
				PrepararJogo();
			});
	}

	public void PrepararJogo(){
		GameObject.FindGameObjectWithTag("Menu").GetComponent<Menu>().SelecionarMissao(this.transform);
		GameObject.FindGameObjectWithTag("Menu").GetComponent<Menu>().PrepararJogo(
			arquivo.GetComponent<TabuleiroInfo>().GetNumMovimentos(),
			arquivo.GetComponent<TabuleiroInfo>().GetX(),
			arquivo.GetComponent<TabuleiroInfo>().GetY(),
			arquivo,
			missaoImgName, cores, numMissao);
	}

}
