﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tipo : MonoBehaviour {
	//UI
	public Text nomeTipoText;
	public Text numCompletadosText;
	public Image imagemTipo;
	public Button button;

	//Dados
	public string nomeTipo;

	public void SetTipo(string nomeTipo, string nomeTipoText, 
					int numCompletados, string nomeImagem, int qtdMissoes){
		this.nomeTipo = nomeTipo;
		this.nomeTipoText.text = nomeTipoText;
		this.numCompletadosText.text = numCompletados + "/" + qtdMissoes;
		imagemTipo.sprite = Resources.Load<Sprite>("Tipos/Imagem/" + nomeImagem);

		button.onClick.AddListener(delegate {
				EscolherMissao();
			});
	}

	public void EscolherMissao(){
		GameObject.FindGameObjectWithTag("Menu").GetComponent<Menu>().SetNomeTipoInicio(nomeTipo);
		GameObject.FindGameObjectWithTag("Menu").GetComponent<Menu>().CarregarMenu(nomeTipo, nomeTipoText.text);
	}
}
