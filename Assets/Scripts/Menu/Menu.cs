﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
	public GameObject inicio;
    public GameObject tiposContainer;

    public GameObject missoesContainer;
    private DBManager banco;
    public GameObject tiposViewer;
    public GameObject missoesViewer;
    public GameObject menuSelecionadoUI;
    public GameObject missaoBloqueadaUI;

    void Start(){
        missoesViewer.SetActive(false);
        tiposViewer.SetActive(false);
        banco = inicio.GetComponent<DBManager>();
        banco.conectarBD();
        CarregarTipos();
    }

    public void PrepararJogo(int numMov, int x, int y, GameObject tabuleiro, string missaoImagem, int cores, int numMissao){
        inicio.GetComponent<Inicio>().SetInfo(numMov, x, y, tabuleiro, missaoImagem, cores, numMissao);
        
    }

    public void MostrarTipos(){
        tiposViewer.SetActive(true);
    }

    public void VoltarMenuPrincipal(){
        tiposViewer.SetActive(false);
    }

    public void SairJogo(){
        Application.Quit();
    }

    public void IniciarJogo(){
        GameObject.DontDestroyOnLoad(inicio);
        StartCoroutine(CarregarJogo());
    }

    IEnumerator CarregarJogo(){
        AsyncOperation load = SceneManager.LoadSceneAsync("Game");
        while(!load.isDone){
            yield return null;
        }
    }

    public void CarregarTipos(){
        List<GameObject> listaTipos = banco.CarregarTiposBD();
        foreach(GameObject tipo in listaTipos){
            tipo.transform.SetParent(tiposContainer.transform);
            tipo.transform.localScale = new Vector3(1,1,1);
        }
    }

    public void CarregarMenu(string nomeTipo, string nomeTipoText){
        missoesViewer.SetActive(true);
        GameObject.FindGameObjectWithTag("NomeTipo").GetComponent<Text>().text = nomeTipoText;
        List<GameObject> listaMissoes = banco.CarregarMissoesBD(nomeTipo);
        foreach(GameObject missao in listaMissoes){
            missao.transform.SetParent(missoesContainer.transform);
            missao.transform.localScale = new Vector3(1,1,1);
        }
    }

    public void SairMissao(){
        menuSelecionadoUI.transform.SetParent(null);
        foreach(Transform child in missoesContainer.transform){
            GameObject.Destroy(child.gameObject);
        }

        missoesViewer.SetActive(false);
    }

    public void SelecionarMissao(Transform missao){
       // menuSelecionadoUI.transform.SetParent(null);
        menuSelecionadoUI.transform.SetParent(missao);
        menuSelecionadoUI.transform.localPosition = new Vector3(0,0,0);
        menuSelecionadoUI.transform.localScale = new Vector3(1,1,1);
    }

    public void bloquearMissao(Transform missao){
        GameObject mBloqueado = Instantiate(missaoBloqueadaUI);
        mBloqueado.transform.SetParent(missao);
        mBloqueado.transform.localPosition = new Vector3(0,0,0);
       // mBloqueado.transform.localScale = new Vector3(1,1,1);
    }

    public void SetNomeTipoInicio(string nomeTipo){
        inicio.GetComponent<Inicio>().nomeTipo = nomeTipo;
    }
}
