﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.SqliteClient;
using System.IO;

public class DBManager : MonoBehaviour {

	private IDbConnection dataBaseConnection;
	
	private string caminhoBD;
	public GameObject tipoPrefab;
	public GameObject missaoPrefab;

	public void conectarBD(){
		caminhoBD = encontrarArquivoBD();
		//Debug.Log(caminhoBD);
		dataBaseConnection = new SqliteConnection (@"URI=File:" + caminhoBD);
		dataBaseConnection.Open();
	}

	public List<GameObject> CarregarTiposBD(){
		List<GameObject> lista = new List<GameObject>();

		IDbCommand dataBaseCommand = dataBaseConnection.CreateCommand ();

		string comando = "SELECT * FROM Tipos";
		dataBaseCommand.CommandText = comando;
		IDataReader busca = dataBaseCommand.ExecuteReader ();

		while(busca.Read()){
			GameObject novoTipo = Instantiate(tipoPrefab);
			novoTipo.GetComponent<Tipo>().SetTipo(busca.GetString(0), busca.GetString(1), busca.GetInt32(3), busca.GetString(4), busca.GetInt32(2));
			lista.Add(novoTipo);
		}

		busca.Close ();
		dataBaseCommand.Dispose ();
		
		return lista;
	}

	public List<GameObject> CarregarMissoesBD(string nomeTipo){
		List<GameObject> lista = new List<GameObject>();

		IDbCommand dataBaseCommand = dataBaseConnection.CreateCommand ();

		string comando = "SELECT * FROM Missoes where NomeTipo LIKE '%" + nomeTipo +"%'";
		dataBaseCommand.CommandText = comando;
		IDataReader busca = dataBaseCommand.ExecuteReader ();

		while(busca.Read()){
			GameObject novaMissao = Instantiate(missaoPrefab);
			int desbloqueado = busca.GetInt32(5);
			bool isDesbloqueado;
			if(desbloqueado == 1){
				isDesbloqueado = true;
			}else{
				isDesbloqueado = false;
			}
			novaMissao.GetComponent<Missao>().SetMissao(busca.GetInt32(0), busca.GetString(1), busca.GetString(2), busca.GetInt32(3), busca.GetString(4), isDesbloqueado, busca.GetInt32(6));
			lista.Add(novaMissao);
		}

		busca.Close ();
		dataBaseCommand.Dispose ();
		
		return lista;
	}

	public void SalvarMissaoConcluida(int numEstrelas){
		int numMissao = GetComponent<Inicio>().numMissao;
		string tipoMissao = GetComponent<Inicio>().nomeTipo;

		IDbCommand dataBaseCommand = dataBaseConnection.CreateCommand();
		string comando = "SELECT NumEstrelas FROM Missoes WHERE NumMissao = "+ numMissao + " AND NomeTipo = '" + tipoMissao+"'";
		dataBaseCommand.CommandText = comando;
		IDataReader busca = dataBaseCommand.ExecuteReader ();
		int numAntigo = 0;
		while(busca.Read()){
			numAntigo = busca.GetInt32(0);
		}

		if(numAntigo < numEstrelas){

		comando = "UPDATE Missoes SET NumEstrelas = " + numEstrelas + " WHERE NumMissao = "+ numMissao + " AND NomeTipo = '" + tipoMissao+"'";
		dataBaseCommand.CommandText = comando;
		dataBaseCommand.ExecuteNonQuery();
		
		int prox = numMissao+1;
		comando = "UPDATE Missoes SET Desbloqueado = 1 WHERE NumMissao = "+ prox + " AND NomeTipo = '" + tipoMissao+"'";
		dataBaseCommand.CommandText = comando;
		dataBaseCommand.ExecuteNonQuery();

		comando = "SELECT NumCompletos FROM Tipos WHERE NomeTipo = '" + tipoMissao+"'";
		dataBaseCommand.CommandText = comando;
		busca = dataBaseCommand.ExecuteReader ();
		int numResolvidos = 0;
		while(busca.Read()){
			numResolvidos = busca.GetInt32(0);
		}
		numResolvidos += 1;
		comando = "UPDATE Tipos SET NumCompletos = " + numResolvidos + " WHERE NomeTipo = '" + tipoMissao+"'";
		dataBaseCommand.CommandText = comando;
		dataBaseCommand.ExecuteNonQuery();

		}


		busca.Close();
		dataBaseCommand.Dispose();
	}

	public void PrepararProximoNivel(){
		int numMissao = GetComponent<Inicio>().numMissao;
		string nomeTipo = GetComponent<Inicio>().nomeTipo;
		IDbCommand dataBaseCommand = dataBaseConnection.CreateCommand ();

		int prox = numMissao+1;
		string comando = "SELECT * FROM Missoes WHERE NumMissao = "+ prox + " AND NomeTipo = '" + nomeTipo +"'";
		dataBaseCommand.CommandText = comando;
		IDataReader busca = dataBaseCommand.ExecuteReader ();
		while(busca.Read()){
			GetComponent<Inicio>().numMissao = prox;
			GetComponent<Inicio>().missaoImagem = busca.GetString(2);
			GetComponent<Inicio>().tabuleiroDesejado = Resources.Load<GameObject>("Missoes/Dado/" + busca.GetString(4));
		}
		GetComponent<Inicio>().ColetarInfoDoArquivo();
		busca.Close();
		dataBaseCommand.Dispose();
	}

	private string encontrarArquivoBD(){
		string path = null;
		if(Application.platform == RuntimePlatform.Android){
			string checkPath = Application.persistentDataPath + "/Banco.db";
			if(!File.Exists(checkPath)){
				WWW loadDB = new WWW("jar:file://"+ Application.dataPath + "!/assets/Banco.db");
					while(!loadDB.isDone){
						//nada
					}
					File.WriteAllBytes(checkPath, loadDB.bytes);
			}
			path = checkPath;
		}else if(Application.platform == RuntimePlatform.WindowsPlayer){
			path = Application.dataPath + "/StreamingAssets/Banco.db";
		}else if(Application.platform == RuntimePlatform.IPhonePlayer){
			path = Application.dataPath + "/Raw/Banco.db";
		}else if(Application.platform == RuntimePlatform.WindowsEditor){
			path = Application.streamingAssetsPath + "/Banco.db";
		}
		return path;
	}



	
}
